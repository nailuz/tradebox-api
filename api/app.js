var express    = require("express");
var mysql      = require('mysql');
var bodyParser = require('body-parser');
var app        = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

var pool = mysql.createPool({
  connectionLimit : 100, //important
  host     : '35.199.102.114',
  user     : 'root',
  password : 'root',
  database : 'tradeBoxDB',
  debug    :  false
});


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // console.log(req['headers']);
  
    next();
  });

  app.listen(8000);

// app.get("/login/:email",function(req,res){
//     var email =  req.params.email;
//     pool.getConnection(function(err,connection){
//         if (err) {
//           res.json({"code" : 100, "status" : "Error in connection database"});
//           return;
//         }          
//         connection.query("SELECT * FROM usuario where email = '"+email+"'",function(err,rows){
//             connection.release();
//             if(!err) {
//                 login = res.json(rows);
//             }         
//         });
//   });
// });

app.get("/login/:email/:senha",function(req,res){
    var email =  req.params.email;
    var senha =  req.params.senha;
    pool.getConnection(function(err,connection){
        if (err) {
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        }          
        connection.query("SELECT * FROM Usuarios where email = '"+email+"' and senha = '"+senha+"'",function(err,rows){
            connection.release();
            if(!err) {
                login = res.json(rows);
            }         
        });
  });
});

app.get("/pedidos/:id",function(req,res){
    var id =  req.params.id;
    console.log("CONSULTANDO PEDIDOS DO USUARIO: ID = ?",req.params.id)
    pool.getConnection(function(err,connection){
        if (err) {
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        } 
        connection.query("SELECT Pedidos.id as pedidoId, Pedidos.data, Pedidos.status, Pedidos.nota, Pedidos.iten_id as itenId, Itens.titulo, Itens.descricao, Itens.preco, Usuarios.nome, Usuarios.celular FROM Pedidos LEFT OUTER JOIN Itens ON Pedidos.iten_id = Itens.id LEFT OUTER JOIN Usuarios ON Itens.vendedor_id = Usuarios.id WHERE Pedidos.comprador_id = ?",id ,function(err,rows){
            connection.release();
            if(!err) {
                pedidos = res.json(rows);
            }          
        });         
  });
});

app.get("/recebidos/:id",function(req,res){
    var id =  req.params.id;
    console.log("CONSULTANDO PEDIDOS RECEBIDOR DO VENDEDOR: ID = ?",req.params.id)
    pool.getConnection(function(err,connection){
        if (err) {
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        } 
        connection.query("SELECT * FROM Pedidos LEFT OUTER JOIN Itens ON Itens.id = Pedidos.iten_id LEFT OUTER JOIN Usuarios ON Pedidos.comprador_id = Usuarios.id WHERE Itens.vendedor_id = ?",id ,function(err,rows){
            connection.release();
            if(!err) {
                res.json(rows);
            }          
        });         
  });
});

app.get("/usuarios",function(req,res){
    console.log("Usuarios")
    pool.getConnection(function(err,connection){
        if (err) {
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        } 
        connection.query("SELECT * FROM Usuarios ",function(err,rows){
            connection.release();
            if(!err) {
                login = res.json(rows);
            }          
        });         
  });
});

app.post("/usuarios",function(req,res){
    console.log("SOLICITANDO UM NOVO CADASTRO:" + req.body.nome);
    let dados =[req.body.nome,req.body.cpf,req.body.celular,req.body.email,req.body.senha,req.body.endereco]

    pool.getConnection(function(err,connection){
        if(err){
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        }
        connection.query("INSERT INTO Usuarios (nome, cpf, celular, email, senha, endereco, pontuacao, vendedor) VALUES (?,?,?,?,?,?, 0, true)", dados,function(err,rows){
            connection.release();
            if(!err) {
                itens = res.json(rows);
            } 
        });
    });
});

app.post("/inserir",function(req,res){
    console.log("INSERINDO NOVO ITEM:" + req.body.titulo);
    let dados =[req.body.tipo, req.body.titulo, req.body.descricao, req.body.preco, req.body.vendedor_id]

    pool.getConnection(function(err,connection){
        if(err){
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        }
        connection.query("INSERT INTO Itens (tipo, titulo, descricao, preco, vendedor_id) VALUES (?,?,?,?,?)", dados,function(err,rows){
            connection.release();
            if(!err) {
                res.json(rows);
            } 
        });
    });
});

app.post("/pedidos",function(req,res){
    console.log("NOVA COMPRA: ",req.body.titulo);
    pool.getConnection(function(err,connection){
        if(err){
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        }
        connection.query("INSERT INTO Pedidos (comprador_id, data, iten_id, status, nota) VALUES (?, NOW(), ?, 'Aguardando', ?)", [req.body.compradorId, req.body.itemId, req.body.nota], function(err,rows){
            connection.release();
            if(!err) {
                itens = res.json(rows);
            } 
        });
    });
});

app.get("/feed",function(req,res){
  console.log("Feed ")
  pool.getConnection(function(err,connection){
      if (err) {
        res.json({"code" : 100, "status" : "Error in connection database"});
        return;
      } 
      var itens
      connection.query("SELECT *, Itens.id as itemId FROM Itens LEFT OUTER JOIN Usuarios ON Itens.vendedor_id = Usuarios.id",function(err,rows){
          connection.release();
          console.log(rows);
          if(!err) {
              itens = res.json(rows);
          }          
      });
  });
});


// app.get("/listUser/:id",function(req,res){
//   var id =  req.params.id;
//   console.log(id);
//   console.log("Feed Usuario")
//   pool.getConnection(function(err,connection){
//       if (err) {
//         res.json({"code" : 100, "status" : "Error in connection database"});
//         return;
//       } 
//       var id = 1;
//       var sql = 'SELECT * FROM Usuarios WHERE id = ?';
//       connection.query(sql, [id], function (err, rows) {
//         if(!err) {
//             itens = res.json(rows);
//         }          
//       });
//   });
// });
